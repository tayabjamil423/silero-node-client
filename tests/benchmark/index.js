const path = require("path");
const fs = require("fs");

const benchmarkUtils = require("@ph/benchmark-utils");

const SileroClient = require("../..");

const sileroClient = new SileroClient({
	port: 5001,
	loggingEnabled: false,
});

const audioFileName = "audio.wav";
const audioFilePath = path.join(__dirname, audioFileName);
const audioFile = fs.readFileSync(audioFilePath);

const getSpeechProbTask = () => {
	const params = {
		wavData: audioFile,
		fileName: audioFileName,
		tag: "test",
	};
	const task = sileroClient.getSpeechProb(params);
	return task;
};

const run = async () => {
	const params = {
		logNamespace: "SileroClient:tests:benchmark",
		name: "getSpeechProbability",
		taskFunc: getSpeechProbTask,
		logOnTaskComplete: false,
	};
	
	await benchmarkUtils.benchmarkTask({ ...params });

	await benchmarkUtils.benchmarkTask({ ...params, maxTaskCount: 1000 });
};

run();
