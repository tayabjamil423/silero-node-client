const path = require("path");
const fs = require("fs");
const debug = require("debug");

const SileroClient = require("../..");

const log = debug("SileroClient:tests:basic");

const sileroClient = new SileroClient({
	port: 5001,
	loggingEnabled: true,
});

const audioFileName = "audio.wav";
const audioFilePath = path.join(__dirname, audioFileName);
const audioFile = fs.readFileSync(audioFilePath);

log("Getting speech prob...");
const params = {
	wavData: audioFile,
	fileName: audioFileName,
	tag: "test",
};
sileroClient
	.getSpeechProb(params)
	.then((data) => {
		log("Got speech prob - data: %O", data);
	})
	.catch((error) => {
		log("Failed to get speech prob - error: %s", error.message);
	});
