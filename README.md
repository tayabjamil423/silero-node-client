# Silero Client

Node.js library for interacting with the [Silero VAD server](https://gitlab.com/tayabjamil423/silero-python).

## Installation
```
npm install git@gitlab.com:tayabjamil423/silero-node-client.git
```

## Usage
```js
const path = require("path");
const fs = require("fs");

// Import Silero Client
const SileroClient = require("silero-client");

// Create client instance
const sileroClient = new SileroClient({
    // Port on which the Silero Server is running
	port: 5001,

    // Determines 
	loggingEnabled: true,
});

// Load audio data
const audioFileName = "audio.wav";
const audioFilePath = path.join(__dirname, audioFileName);
const audioFile = fs.readFileSync(audioFilePath);

// Initiate request
const result = await sileroClient.getSpeechProbability(audioFile, audioFileName, "test");

// Log result
console.log(result);
```

**Example result:**
```
{
    "average_speech_probability": 0.8855901460163296,
    "success": true
}
```