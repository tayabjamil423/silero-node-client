const debug = require("debug");
const lodash = require("lodash");
const now = require("performance-now");
const Joi = require("joi");
//
const FormData = require("form-data");
const axios = require("axios").default;
const axiosRetry = require("axios-retry");

class SileroClient {
	static _type = "SileroClient";

	log = null;

	options = null;

	client = null;

	static configureOptionsSchema = Joi.object({
		baseUrl: Joi.string().default("http://127.0.0.1"),
		speechProbRoute: Joi.string().default("speech_probability"),
		port: Joi.number().default(5001),

		clientOptions: Joi.object({
			timeout: Joi.number().default(1200),
		})
			.unknown()
			.default(),

		retryOptions: Joi.object({
			retries: Joi.number().default(1),
			retryDelay: Joi.func().default(() => {
				return () => {
					return 500;
				};
			}),
		})
			.unknown()
			.default(),

		loggingEnabled: Joi.bool().default(false),
	});
	static typeSchema = Joi.object({
		_type: Joi.string().equal(SileroClient._type),
	}).unknown();
	constructor(options) {
		options = Joi.attempt(
			options,
			SileroClient.configureOptionsSchema.default()
		);

		const { clientOptions, retryOptions, loggingEnabled } = options;

		// Create logger
		const log = debug("SileroClient");
		log.enabled = loggingEnabled;
		this.log = log;

		log("Creating - options: %O", options);

		// Create client
		const client = axios.create(clientOptions);
		this.client = client;

		// Init axios-retry
		axiosRetry(client, retryOptions);

		this.options = options;

		log("Created");
	}

	static getSpeechProbParamsSchema = Joi.object({
		wavData: Joi.binary().required(),
		fileName: Joi.string().required(),
		tag: Joi.string().required(),
		requestOptions: Joi.object({
			"axios-retry": Joi.object({
				retries: Joi.number().optional(),
			})
				.unknown()
				.optional(),
			timeout: Joi.number().optional(),
		})
			.unknown()
			.default(),
	}).default();
	async getSpeechProb(params) {
		params = Joi.attempt(params, SileroClient.getSpeechProbParamsSchema);

		// Destructure operation vars
		const { wavData, fileName, tag, requestOptions } = params;

		// Create method-level log
		const baseLog = this.log;
		const methodLog = baseLog.extend("getSpeechProb");
		methodLog.enabled = baseLog.enabled;

		// Create tagged log
		const tagLog = methodLog.extend(tag);
		tagLog.enabled = methodLog.enabled;

		// Elapsed time vars
		const startTimeMs = now();
		const getElapsedTimeMs = () => {
			const endTimeMs = now();
			let elapsedTimeMs = endTimeMs - startTimeMs;
			elapsedTimeMs = Math.floor(elapsedTimeMs);
			return elapsedTimeMs;
		};

		const speechProbUrl = this.getSpeechProbUrl();

		tagLog(
			"Requesting speech probability - params: %O, speechProbUrl: %s",
			params,
			speechProbUrl
		);

		try {
			// Create form data
			const formData = new FormData();
			formData.append("audio_file", wavData, {
				contentType: "audio/wav",
				knownLength: wavData.length,
				filename: fileName,
			});

			formData.append("tag", tag);

			const formHeaders = formData.getHeaders();

			// Initiate request
			const response = await this.client.post(speechProbUrl, formData, {
				headers: { ...formHeaders, Accept: "application/json" },
				...requestOptions,
			});

			const elapsedTimeMs = getElapsedTimeMs();

			const data = response.data;

			tagLog(
				"🟢 Request successful - data: %O, elapsedTimeMs: %dms",
				data,
				elapsedTimeMs
			);

			const result = {
				success: true,
				data: data,
				elapsedTimeMs: elapsedTimeMs,
			};

			return result;
		} catch (error) {
			const elapsedTimeMs = getElapsedTimeMs();

			const errorMessage = error.message;
			const data = error.response?.data;

			tagLog(
				"🔴 Request failed - error: %s, data: %O, elapsedTimeMs: %dms",
				error.message,
				data,
				elapsedTimeMs
			);

			const result = {
				success: false,
				data: data,
				elapsedTimeMs: elapsedTimeMs,
			};

			error.result = result;

			throw error;
		}
	}

	getServerUrl() {
		const options = this.options;
		const { baseUrl, port } = options;
		const url = `${baseUrl}:${port}`;
		return url;
	}

	getSpeechProbUrl() {
		const serverUrl = this.getServerUrl();
		const speechProbRoute = this.options.speechProbRoute;
		return `${serverUrl}/${speechProbRoute}`;
	}
}

module.exports = SileroClient;
